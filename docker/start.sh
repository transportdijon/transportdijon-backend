#!/bin/bash

pip install --upgrade pip
pip install -r /app/requirements.txt
pip install gunicorn


gunicorn td_backend:api -c /app/gunicorn_conf.py --reload