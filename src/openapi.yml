openapi: "3.0.2"
info:
  title: Transport Dijon Backend
  version: "1.0"
  contact:
    email: "android@tsuna.fr"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
servers:
  - url: https://pp.transportdijon.tsuna.fr/services
  - url: https://pp2.transportdijon.tsuna.fr/services

components:
  schemas:
    version:
      description: Object contenant la version de l'appli
      type: object
      properties:
        version: 
          type: string
        changelog:
          type: string
        
    message:
      description: Message d'information
      type: object
      properties:
        id:
          type: integer
        criticality:
          type: string
        title:
          type: string
        message:
          type: string
        author:
          type: string
        createdAt:
          type: string
        updatedAt:
          type: string
    messages:
      description: Tableau avec l'ensemble des messages d'information
      type: array
      items:
        $ref: "#/components/schemas/message"
    city:
      description: Détail d'une ville
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
    cities:
      description: Tableau avec l'ensemble des villes disponible
      type: array
      items:
        $ref: "#/components/schemas/city"
    line:
      description: Information sur les directions possible d'une ligne
      type: object
      properties:
        code:
          type: string
        nom:
          type: string
        sens:
          type: string
        vers:
          type: string
        couleur:
          type: integer

    lines:
      description: Tableau avec l'ensemble des lignes disponible
      type: array
      items:
        $ref: "#/components/schemas/line"
    station:
      description: ""
      type: object
      properties:
        code:
          type: integer
        nom:
          type: string
        refs:
          type: string
    stations:
      description: ""
      type: array
      items:
        $ref: "#/components/schemas/station"
    time:
      description: ""
      type: object
      properties:
        time:
          type: integer
        destination:
          type: string
    next:
      description: ""
      type: array
      items:
        $ref: "#/components/schemas/time"

paths:
  /version:
    get:
      responses:
        "200":
          description: Renvoie la version actuel de l'appli
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/version"
  /V1.0/messages:
    get:
      responses:
        "200":
          description: Liste des annonces en cours
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/messages"
        "204":
          description: Pas de message
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/cities:
    get:
      responses:
        "200":
          description: Liste des villes disponible
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/cities"
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/cities/{city_name}:
    get:
      parameters:
        - in: path
          name: city_name
          schema:
            type: string
          required: true
          description: Nom de la ville à récupérer
          example: "Dijon"

      responses:
        "200":
          description: Information sur la ville demandé
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/city"
        "404":
          description: ville non disponible
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/lines/{city_id}:
    get:
      parameters:
        - in: path
          name: city_id
          schema:
            type: integer
          required: true
          description: ID de la ville souhaité
          example: "217"
      responses:
        "200":
          description: Liste des lignes disponible
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/lines"
        "204":
          description: Pas de lignes pour la ville demandé
        "400":
          description: Erreur de format, les arguments passés à la requête ne sont pas au bon format
        "404":
          description: La ville demandé n'existe pas
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/lines/{city_id}/{line}:
    get:
      parameters:
        - in: path
          name: city_id
          schema:
            type: integer
          required: true
          description: ID de la ville souhaité
          example: "217"
        - in: path
          name: line
          schema:
            type: string
          required: true
          description: Code de la ligne demandé
          example: "T1"
      responses:
        "200":
          description: Liste des direction disponible pour cette ligne
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/lines"
        "204":
          description: Pas de lignes pour la ville demandé
        "400":
          description: Erreur de format, les arguments passés à la requête ne sont pas au bon format
        "404":
          description: La ville demandé n'existe pas
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/lines/{city_id}/{line}/{direction}:
    get:
      parameters:
        - in: path
          name: city_id
          schema:
            type: integer
          required: true
          description: ID de la ville souhaité
          example: "217"
        - in: path
          name: line
          schema:
            type: string
          required: true
          description: Code de la ligne demandé
          example: "T1"
        - in: path
          name: direction
          schema:
            type: string
          required: true
          description: direction de la ligne demandé
          example: "A"
      responses:
        "200":
          description: Liste des direction disponible pour cette ligne
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/line"
        "204":
          description: Pas de lignes pour la ville demandé
        "400":
          description: Erreur de format, les arguments passés à la requête ne sont pas au bon format
        "404":
          description: La ville demandé n'existe pas
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/stations/{city_id}/{line}/{direction}:
    get:
      parameters:
        - in: path
          name: city_id
          schema:
            type: integer
          required: true
          description: ID de la ville souhaité
          example: "217"
        - in: path
          name: line
          schema:
            type: string
          required: true
          description: Code de la ligne demandé
          example: "T1"
        - in: path
          name: direction
          schema:
            type: string
          required: true
          description: Sens de la ligne demandé
          example: "A"
      responses:
        "200":
          description: Liste des arrêts disponible pour la ligne dans le sens demandé
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/stations"
        "204":
          description: Pas d'arrêt pour la ligne demandée
        "400":
          description: Erreur de format, les arguments passés à la requête ne sont pas au bon format
        "404":
          description: La ville/ligne demandée n'existe pas
        "406":
          description: Votre client demande un format différent du application/json
  /V1.0/stations/{city_id}/{line}/{direction}/{station}:
    get:
      parameters:
        - in: path
          name: city_id
          schema:
            type: integer
          required: true
          description: ID de la ville souhaité
          example: "217"
        - in: path
          name: line
          schema:
            type: string
          required: true
          description: Code de la ligne demandé
          example: "T1"
        - in: path
          name: direction
          schema:
            type: string
          required: true
          description: Sens de la ligne demandé
          example: "A"
        - in: path
          name: station
          schema:
            type: integer
          required: true
          description: Code de la station à consulter (Code Totem)
          example: "1542"
      responses:
        "200":
          description: Renvoie les temps d'attentes avant les prochains passages
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/next"
        "204":
          description: Pas de temps d'attente pour l'arrêt demandé
        "400":
          description: Erreur de format, les arguments passés à la requête ne sont pas au bon format
        "404":
          description: La ville/ligne/station demandée n'existe pas
        "406":
          description: Votre client demande un format différent du application/json
