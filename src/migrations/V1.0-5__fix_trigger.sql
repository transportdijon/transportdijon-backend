CREATE OR REPLACE FUNCTION updateDate() RETURNS TRIGGER
    LANGUAGE plpgsql
as
$$
BEGIN
    NEW.updatedAt = NOW();
    RETURN NEW;
END;
$$;