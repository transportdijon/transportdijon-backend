-- Création des tables
CREATE TABLE city
(
    id        INTEGER   NOT NULL PRIMARY KEY,
    nom       VARCHAR(100),
    active    BOOLEAN   NOT NULL DEFAULT TRUE,
    createdAt TIMESTAMP NOT NULL DEFAULT NOW(),
    updatedAt TIMESTAMP NOT NULL DEFAULT NOW()
) WITH (OIDS = FALSE);

CREATE TABLE line
(
    id          INTEGER      NOT NULL,
    name        VARCHAR(100) NOT NULL,
    code        VARCHAR(100) NOT NULL,
    sens        VARCHAR(100) NOT NULL CHECK ( sens IN ('A', 'R') ),
    destination VARCHAR(100) NOT NULL,
    color       VARCHAR(50),
    city_id     INTEGER      NOT NULL REFERENCES city (id) on update cascade on delete restrict,
    active      BOOLEAN      NOT NULL DEFAULT TRUE,
    createdAt   TIMESTAMP    NOT NULL DEFAULT NOW(),
    updatedAt   TIMESTAMP    NOT NULL DEFAULT NOW(),
    primary key(id,city_id)
) WITH (OIDS = FALSE);

CREATE TABLE station
(
    id                 INTEGER      NOT NULL PRIMARY KEY,
    nom                VARCHAR(100) NOT NULL,
    line_id            INTEGER      NOT NULL,
    city_id            INTEGER      NOT NULL,
    schedule_reference VARCHAR(200),
    active             BOOLEAN      NOT NULL DEFAULT TRUE,
    createdAt          TIMESTAMP    NOT NULL DEFAULT NOW(),
    updatedAt          TIMESTAMP    NOT NULL DEFAULT NOW(),
    FOREIGN KEY (line_id, city_id) REFERENCES line (id, city_id)
) WITH (OIDS = FALSE);

CREATE TABLE message
(
    id          SERIAL       NOT NULL PRIMARY KEY,
    criticality VARCHAR(50)  NOT NULL,
    title       VARCHAR(50)  NOT NULL,
    message     VARCHAR(500) NOT NULL,
    author      VARCHAR(50),
    beginAt     Date,
    endAt       Date,
    active      BOOLEAN      NOT NULL DEFAULT TRUE,
    createdAt   TIMESTAMP    NOT NULL DEFAULT NOW(),
    updatedAt   TIMESTAMP    NOT NULL DEFAULT NOW()
) WITH (OIDS = FALSE);

CREATE TABLE "user"
(
    id        SERIAL       NOT NULL PRIMARY KEY,
    mail      VARCHAR(200) NOT NULL UNIQUE,
    favorites JSON,
    active    BOOLEAN      NOT NULL DEFAULT TRUE,
    createdAt TIMESTAMP    NOT NULL DEFAULT NOW(),
    updatedAt TIMESTAMP    NOT NULL DEFAULT NOW()
) WITH (OIDS = FALSE);

CREATE TABLE feedback
(
    id        SERIAL    NOT NULL PRIMARY KEY,
    user_id   INTEGER REFERENCES "user" (id) on update cascade on delete cascade,
    score     NUMERIC,
    message   VARCHAR,
    createdAt TIMESTAMP NOT NULL DEFAULT NOW()
) WITH (OIDS = FALSE);

-- Création des triggers
CREATE FUNCTION updateDate() RETURNS TRIGGER
    LANGUAGE plpgsql
as
$$
BEGIN
    NEW.updatedAt = NOW();
END;
$$;
CREATE TRIGGER "99_updateDate"
    BEFORE UPDATE
    ON city
    FOR EACH ROW
EXECUTE PROCEDURE updateDate();
CREATE TRIGGER "99_updateDate"
    BEFORE UPDATE
    ON line
    FOR EACH ROW
EXECUTE PROCEDURE updateDate();
CREATE TRIGGER "99_updateDate"
    BEFORE UPDATE
    ON station
    FOR EACH ROW
EXECUTE PROCEDURE updateDate();
CREATE TRIGGER "99_updateDate"
    BEFORE UPDATE
    ON message
    FOR EACH ROW
EXECUTE PROCEDURE updateDate();

CREATE TRIGGER "99_updateDate"
    BEFORE UPDATE
    ON "user"
    FOR EACH ROW
EXECUTE PROCEDURE updateDate();

-- Création des index
CREATE INDEX ON line (sens);
CREATE INDEX ON line (code);
CREATE INDEX ON line (city_id);
CREATE INDEX ON station (line_id);
CREATE INDEX ON feedback (user_id);
