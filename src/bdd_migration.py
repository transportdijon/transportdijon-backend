# -*- coding: utf-8 -*-
"""Module de gestion de la BDD (installation + mise à jour) pour l'application Transport Dijon"""

import json
from yoyo import read_migrations, get_backend
from config import Config


def main():

    db_config = Config.configuration['database']

    backend = get_backend(db_config['type'] + '://' + db_config['username'] + ':' +
                          db_config['password'] + '@' + db_config['host'] + '/' + db_config['name'] + '')
    migrations = read_migrations('migrations')
    backend.apply_migrations(backend.to_apply(migrations))
