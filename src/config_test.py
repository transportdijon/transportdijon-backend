
from config import Config
import pytest


def test_open_default_config():
    config = Config()
    config.load()


def test_open_parametrized_config():
    config = Config()
    config.load("../test/config.json")


def test_open_wrong_json_config():
    config = Config()
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        config.load("../test/config.nojson")
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 2


def test_open_missing_config():
    config = Config()
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        config.load("../test/config.noexists")
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1


def test_open_empty_config():
    config = Config()
    config.load("../test/empty.json")


def test_clear_empty_config():
    config = Config()
    config.clear()


def test_clear_default_config():
    config = Config()
    config.load()
    config.clear()
