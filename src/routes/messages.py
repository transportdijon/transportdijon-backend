# -*- coding: utf-8 -*-
"""
Module backend pour l'application Transport Dijon

Ce module sert d'interface entre l'API keolis et les applications déployées sur Android
"""

import json
import falcon
from postgres_pool import PostgresPool


class MessageRouteV1:  # pylint: disable=too-few-public-methods
    """Classe gérant les routes concernant les messages"""

    @staticmethod
    def on_get(req: falcon.request.Request, resp: falcon.response.Response):
        """
        Handles GET request on route /messages
        Retourne la liste des messages actifs dans la base
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse
            messages = []

            conn = PostgresPool.get_connexion()

            curs = conn.cursor()
            curs.execute("SELECT * FROM message WHERE active;")
            rows = curs.fetchall()
            for row in rows:
                messages.append({
                    'id': row[0],
                    'criticality': row[1],
                    'title': row[2],
                    'message': row[3],
                    'author': row[4],
                    'createdAt': str(row[5]),
                    'updatedAt': str(row[6])
                })

            curs.close()
            PostgresPool.put_connexion(conn)

            if not messages:
                # Si il n'y a aucun message, alors on retourne le code HTTP 204
                resp.status = falcon.HTTP_204
                resp.body = ""
            else:
                resp.status = falcon.HTTP_OK
                resp.content_type = falcon.MEDIA_JSON
                resp.body = (json.dumps(messages))
        else:
            resp.status = falcon.HTTP_406
