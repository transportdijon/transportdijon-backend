# -*- coding: utf-8 -*-
"""
Module backend pour l'application Transport Dijon

Ce module sert d'interface entre l'API keolis et les applications déployées sur Android
"""

import json
import falcon


class VersionRoute:  # pylint: disable=too-few-public-methods
    """Classe gérant les routes concernant les versions"""

    @staticmethod
    def on_get(req: falcon.request.Request, resp: falcon.response.Response):
        """
        Handles GET request on route /version
        Retourne la version actuel de l'api
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse
            messages = {'version' : '1.0', 'changelog' : "Création de l'api"}
            resp.status = falcon.HTTP_OK
            resp.content_type = falcon.MEDIA_JSON
            resp.body = (json.dumps(messages))
        else:
            resp.status = falcon.HTTP_406
