# -*- coding: utf-8 -*-
"""
Module backend pour l'application Transport Dijon

Ce module sert d'interface entre l'API keolis et les applications déployées sur Android
"""

import json
import falcon
from postgres_pool import PostgresPool


class LinesRouteV1:
    """Classe gérant les routes concernant les lignes"""

    @staticmethod
    def on_get_all(req: falcon.request.Request, resp: falcon.response.Response, city_id: str):
        """
        Handles GET request on route /lines/{city_id}
        Retourne la liste des villes dans la base
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse

            if not city_id.isnumeric():
                # On vérifie que les arguments entiers passés en paramètres le sont
                resp.status = falcon.HTTP_400
                resp.body = ""
                return

            lines = []

            conn = PostgresPool.get_connexion()

            curs = conn.cursor()

            # on vérifie que la ville demandé existe
            curs.execute("SELECT nom FROM city WHERE active AND id = '{city_id}' ORDER BY id;".format(
                city_id=city_id))
            city_rows = curs.fetchall()
            if len(city_rows) == 0:
                resp.status = falcon.HTTP_404
                resp.body = ""
            else:

                curs.execute("SELECT code, name, sens, destination, color FROM line WHERE active AND city_id = '{city_id}' ORDER BY id;".format(
                    city_id=city_id))
                rows = curs.fetchall()
                for row in rows:
                    lines.append({
                        'code': row[0],
                        'nom': row[1],
                        'sens': row[2],
                        'vers': row[3],
                        'couleur': int(row[4])
                    })

                if not lines:
                    # Si il n'y a aucune ligne pour cette ville, alors on retourne le code HTTP 204
                    resp.status = falcon.HTTP_204
                    resp.body = ""
                else:
                    resp.status = falcon.HTTP_OK
                    resp.body = (json.dumps(lines))
            curs.close()
            PostgresPool.put_connexion(conn)
        else:
            resp.status = falcon.HTTP_406

    @staticmethod
    def on_get_one(req: falcon.request.Request, resp: falcon.response.Response, city_id: str, line: str, direction: str):
        """
        Handles GET request on route /lines/{city_id}/{direction}
        Retourne la liste des villes dans la base
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse

            if not city_id.isnumeric():
                # On vérifie que les arguments entiers passés en paramètres le sont
                resp.status = falcon.HTTP_400
                resp.body = ""
                return

            conn = PostgresPool.get_connexion()

            curs = conn.cursor()

            # on vérifie que la ville demandé existe
            curs.execute("SELECT nom FROM city WHERE active AND id = '{city_id}' ORDER BY id;".format(
                city_id=city_id))
            city_rows = curs.fetchall()
            if len(city_rows) == 0:
                resp.status = falcon.HTTP_404
                resp.body = ""
            else:

                curs.execute("SELECT code, name, sens, destination, color " +
                             "FROM line " +
                             "WHERE active " +
                             "AND city_id = '{city_id}' AND code = '{line}' AND sens = '{sens}' ORDER BY id;".format(
                                 city_id=city_id, line=line, sens=direction))
                row = curs.fetchone()
                if not row:
                    # Si il n'y a aucune ligne pour cette ville, alors on retourne le code HTTP 204
                    resp.status = falcon.HTTP_204
                    resp.body = ""

                else:
                    line = {
                        'code': row[0],
                        'nom': row[1],
                        'sens': row[2],
                        'vers': row[3],
                        'couleur': int(row[4])
                    }
                    resp.status = falcon.HTTP_OK
                    resp.body = (json.dumps(line))
            curs.close()
            PostgresPool.put_connexion(conn)
        else:
            resp.status = falcon.HTTP_406

    @staticmethod
    def on_get(req: falcon.request.Request, resp: falcon.response.Response, city_id: str, line: str):
        """
        Handles GET requests on route /cities/{city_id}/{line}
        Retourne les informations concernant la ville passée en paramètre
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse

            if not city_id.isnumeric():
                # On vérifie que les arguments entiers passés en paramètres le sont
                resp.status = falcon.HTTP_400
                resp.body = ""
                return

            conn = PostgresPool.get_connexion()

            curs = conn.cursor()
            curs.execute("SELECT code, name, sens, destination, color FROM line WHERE active AND city_id = '{city_id}' AND code = '{line}' ORDER BY id;".format(
                city_id=city_id, line=line))
            rows = curs.fetchall()

            curs.close()
            PostgresPool.put_connexion(conn)
            lines = []
            if len(rows) == 0:
                # Si il n'y a aucune ville, alors on retourne le code HTTP 404
                resp.status = falcon.HTTP_404
                resp.body = ""
            else:
                # HTTP Code 200
                resp.status = falcon.HTTP_OK
                resp.content_type = falcon.MEDIA_JSON
                for row in rows:
                    lines.append({
                        'code': row[0],
                        'nom': row[1],
                        'sens': row[2],
                        'vers': row[3],
                        'couleur': int(row[4])
                    })
                resp.body = json.dumps(lines)
        else:
            resp.status = falcon.HTTP_406
