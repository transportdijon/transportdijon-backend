# -*- coding: utf-8 -*-
"""
Module backend pour l'application Transport Dijon

Ce module sert d'interface entre l'API keolis et les applications déployées sur Android
"""
import math
from datetime import datetime
from datetime import timedelta
import json
import requests
import falcon
from bs4 import BeautifulSoup
from postgres_pool import PostgresPool


class StationRouteV1:
    """Classe gérant les routes concernant les arrêts"""

    @staticmethod
    def on_get_all(req: falcon.request.Request, resp: falcon.response.Response, city_id: str, line: str,
                   direction: str):
        """
        Handles GET request on route /stations/{city_id}/{line}/{direction}
        Retourne la liste des arrêts d'une ligne
        """
        if not req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse
            resp.status = falcon.HTTP_406
            return

        if not city_id.isnumeric():
            # On vérifie que les arguments entiers passés en paramètres le sont
            resp.status = falcon.HTTP_400
            resp.body = ""
            return

        stations = []

        conn = PostgresPool.get_connexion()

        curs = conn.cursor()

        # on vérifie que la ville demandé existe
        curs.execute("SELECT nom FROM city WHERE active AND id = '{city_id}' ORDER BY id;".format(city_id=city_id))
        city_rows = curs.fetchall()
        if len(city_rows) == 0:
            resp.status = falcon.HTTP_404
            resp.body = ""
        else:

            # On vérifie que la ligne existe
            curs.execute(
                "SELECT id FROM line where active AND city_id = '{city_id}' AND code = '{line}' AND sens = '{sens}'".format(
                    city_id=city_id, line=line, sens=direction))
            line_rows = curs.fetchall()

            if len(line_rows) == 0:
                resp.status = falcon.HTTP_404
                resp.body = ""
            else:
                curs.execute(
                    "SELECT code, nom, refs FROM station WHERE active AND city_id = '{city_id}' AND line_id = {line_id} ORDER BY id;".format(
                        city_id=city_id, line_id=line_rows[0][0]))
                rows = curs.fetchall()
                for row in rows:
                    stations.append({
                        'code': row[0],
                        'nom': row[1],
                        'refs': row[2]
                    })

                if not stations:
                    # Si il n'y a aucun arrêt pour cette ligne, alors on retourne le code HTTP 204
                    resp.status = falcon.HTTP_204
                    resp.body = ""
                else:
                    resp.status = falcon.HTTP_OK
                    resp.body = (json.dumps(stations))
        curs.close()
        PostgresPool.put_connexion(conn)

    @staticmethod
    def on_get(req: falcon.request.Request, resp: falcon.response.Response, city_id: str, line: str, direction: str, station: str):  # pylint: disable=too-many-arguments
        """
        Handles GET request on route /stations/{city_id}/{line}/{direction}/{station}
        Retourne la liste des arrêts d'une ligne
        """
        if not req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse
            resp.status = falcon.HTTP_406
            return

        if not city_id.isnumeric() or not station.isnumeric():
            # On vérifie que les arguments entiers passés en paramètres le sont
            resp.status = falcon.HTTP_400
            resp.body = ""
            return

        nexts = []

        conn = PostgresPool.get_connexion()
        curs = conn.cursor()

        # on vérifie que la ville demandée existe
        curs.execute("SELECT nom FROM city WHERE active AND id = '{city_id}' ORDER BY id;".format(city_id=city_id))
        rows = curs.fetchall()
        if len(rows) == 0:
            resp.status = falcon.HTTP_404
            resp.body = ""
        else:
            # On vérifie que la ligne existe
            curs.execute(
                "SELECT id FROM line where active AND city_id = '{city_id}' AND code = '{line}' AND sens = '{sens}'".format(
                    city_id=city_id, line=line, sens=direction))
            rows = curs.fetchall()

            if len(rows) == 0:
                resp.status = falcon.HTTP_404
                resp.body = ""
            else:
                # On vérifie que la station existe
                curs.execute(
                    "SELECT refs FROM station WHERE active AND city_id = '{city_id}' AND line_id = {line_id} AND code = {code} ORDER BY id;".format(
                        city_id=city_id, line_id=rows[0][0], code=station))
                rows = curs.fetchall()
                if len(rows) == 0:
                    resp.status = falcon.HTTP_404
                    resp.body = ""
                else:
                    content = requests.get("http://timeo3.keolis.com/relais/{city_id}.php?xml=3&refs={refs}&ran=1".format(refs=rows[0][0], city_id=city_id)).text
                    soup = BeautifulSoup(content, 'lxml')

                    if soup.find('passages')['nb'] == '0':
                        # Si il n'y a aucun prochain passage pour cet arrêt, alors on retourne le code HTTP 204
                        resp.status = falcon.HTTP_204
                        resp.body = ""
                    else:
                        # Sinon on itère sur les passages pour obtenir les horaires
                        for nextstop in soup.find_all('passage'):
                            date_time: datetime = datetime.strptime(soup.date.text + " " + nextstop.duree.text, '%Y-%m-%d %H:%M')
                            nexts.append(
                                {
                                    'time': StationRouteV1.get_time_remaining(date_time),
                                    'destination': nextstop.destination.text
                                }
                            )

                        resp.status = falcon.HTTP_200
                        resp.body = json.dumps(nexts)
        curs.close()
        PostgresPool.put_connexion(conn)

    @staticmethod
    def get_time_remaining(schedultime: datetime, departuretime: datetime = datetime.now()) -> int:
        """
        Fonction de calcul de temps entre deux dates
        :param schedultime: temps de l'horaire
        :param departuretime: temps actuel ou de référence
        :return: temps restant en minute
        """
        depart = departuretime - timedelta(seconds=departuretime.second, microseconds=departuretime.microsecond)
        schedule = schedultime - timedelta(seconds=schedultime.second, microseconds=schedultime.microsecond)
        time: timedelta = schedule - depart
        if time < timedelta():
            time += timedelta(days=1)
        return math.floor(time.seconds / 60)
