# -*- coding: utf-8 -*-
"""
Module backend pour l'application Transport Dijon

Ce module sert d'interface entre l'API keolis et les applications déployées sur Android
"""

import json
import falcon
from postgres_pool import PostgresPool


class CitiesRouteV1:
    """Classe gérant les routes concernant les villes"""

    @staticmethod
    def on_get_all(req: falcon.request.Request, resp: falcon.response.Response):
        """
        Handles GET request on route /cities
        Retourne la liste des villes dans la base
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse
            cities = []

            conn = PostgresPool.get_connexion()

            curs = conn.cursor()
            curs.execute("SELECT id, nom FROM city WHERE active ORDER BY id;")
            rows = curs.fetchall()
            for row in rows:
                cities.append({
                    'id': row[0],
                    'name': row[1]
                })

            curs.close()
            PostgresPool.put_connexion(conn)

            if not cities:
                # Si il n'y a aucune ville, alors on retourne le code HTTP 204
                resp.status = falcon.HTTP_204
                resp.body = ""
            else:
                resp.status = falcon.HTTP_OK
                resp.body = (json.dumps(cities))
        else:
            resp.status = falcon.HTTP_406

    @staticmethod
    def on_get(req: falcon.request.Request, resp: falcon.response.Response, city_name: str):
        """
        Handles GET requests on route /cities/{city_name}
        Retourne les informations concernant la ville passée en paramètre
        """
        if req.client_accepts_json:
            # On vérifie que le client accepte le json en réponse
            conn = PostgresPool.get_connexion()

            curs = conn.cursor()
            curs.execute(
                "SELECT id, nom FROM city WHERE nom = %s;", (city_name,))
            row = curs.fetchone()

            curs.close()
            PostgresPool.put_connexion(conn)
            if row is None:
                # Si il n'y a aucune ville, alors on retourne le code HTTP 404
                resp.status = falcon.HTTP_404
                resp.body = ""
            else:
                # HTTP Code 200
                resp.status = falcon.HTTP_OK
                resp.content_type = falcon.MEDIA_JSON
                message = {'id': row[0], 'name': row[1]}
                resp.body = json.dumps(message)
        else:
            resp.status = falcon.HTTP_406
