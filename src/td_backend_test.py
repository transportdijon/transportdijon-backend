"""Mode de test unitaire de l'api transport dijon Backend"""
import json

import falcon
import psycopg2
import pytest
from falcon import testing
from datetime import datetime, timedelta

from postgres_pool import PostgresPool
from routes.stations import StationRouteV1
from td_backend import api

TEST_VERSION = "/V1.0"
ROUTE_MESSAGES = "/messages"
ROUTE_CITIES = "/cities"
ROUTE_VERSION = "/version"
ROUTE_LINES = "/lines"
ROUTE_STATIONS = "/stations"

XML_ONLY_HEADER = {'Accept': 'application/xml'}

LIGNE_T1_A = {"code": "T1", "nom": "T1", "sens": "A",
              "vers": "QUETIGNY Centre", "couleur": 13369548}
LIGNE_T1_R = {"code": "T1", "nom": "T1", "sens": "R",
              "vers": "DIJON Gare", "couleur": 13369548}
LIGNE_T2_A = {"code": "T2", "nom": "T2", "sens": "A",
              "vers": "DIJON Valmy", "couleur": 13369548}
LIGNE_T2_R = {"code": "T2", "nom": "T2", "sens": "R",
              "vers": "CHENOVE Centre", "couleur": 13369548}
LIGNE_COROL_A = {"code": "C", "nom": "Corol", "sens": "A",
                 "vers": "Marmuzots", "couleur": 13382451}
LIGNE_COROL_R = {"code": "C", "nom": "Corol", "sens": "R",
                 "vers": "Fontaine d&#39;Ouche", "couleur": 13382451}
LIGNE_CITY_A = {"code": "DIV", "nom": "City", "sens": "A",
                "vers": "Tivoli par Darcy", "couleur": 13369395}
LIGNE_CITY_R = {"code": "DIV", "nom": "City", "sens": "R",
                "vers": "République", "couleur": 13369395}

datetimeformat = '%Y-%m-%d %H:%M'
now: datetime = datetime.now()
today: str = now.strftime('%Y-%m-%d')


@pytest.fixture(autouse=True)
def run_around_tests():
    """ Test de vérification que l'ensemble des connexions sont bien rendues au pool """
    starting_conn = PostgresPool.get_used_connection()
    yield
    assert PostgresPool.get_used_connection(
    ) == starting_conn, "C'est pas bien t'as pas fermé ta connection à la BDD"


@pytest.fixture(name='client')
def fixiture_client():
    return testing.TestClient(api)


def test_bdd_ko():
    """ test de gestion d'erreur lors de la connecion à la bdd """

    PostgresPool.close()
    with pytest.raises(psycopg2.DatabaseError):
        PostgresPool.init_pool_postgresql('../test/config_bdd_ko.json')

    PostgresPool.init_pool_postgresql()


def test_init_api():
    """ Vérification de la création de l'objet api"""
    assert api is not None


def test_messages_one_msg(client):
    """ Test de la route /messages : Mode 1 message disponible """
    response = client.simulate_get(TEST_VERSION + ROUTE_MESSAGES)
    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert len(result) == 1
    assert result[0]['id'] == 1
    assert result[0]['criticality'] == 'information'
    assert result[0]['title'] == 'nouvelle application'
    assert result[0]['message'] == 'Bienvenue sur la nouvelle application Transport Dijon'
    assert result[0]['author'] == 'Tsuna'


def test_messages_no_msg(client):
    """ Test de la route /messages : Pas de message disponible disponible """
    conn = PostgresPool.get_connexion()
    curs = conn.cursor()
    curs.execute("UPDATE message SET active = false WHERE id = 1;")
    conn.commit()

    response = client.simulate_get(TEST_VERSION + ROUTE_MESSAGES)

    curs.execute("UPDATE message SET active = true WHERE id = 1;")
    conn.commit()
    curs.close()
    PostgresPool.put_connexion(conn)

    assert response.status == falcon.HTTP_204
    assert response.content == b''


def test_messages_406_error(client):
    """ Test en cas de retour json incompatible """
    response = client.simulate_get(
        TEST_VERSION + ROUTE_MESSAGES, headers=XML_ONLY_HEADER)
    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_cities(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_CITIES)
    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert len(result) == 15
    assert result[0]['id'] == 105
    assert result[0]['name'] == 'Le mans'
    assert result[5]['id'] == 217
    assert result[5]['name'] == 'Dijon'


def test_cities_no_city(client):
    conn = PostgresPool.get_connexion()
    curs = conn.cursor()
    curs.execute("UPDATE city SET active = false;")
    conn.commit()

    response = client.simulate_get(TEST_VERSION + ROUTE_CITIES)

    curs.execute("UPDATE city SET active = true;")
    conn.commit()
    curs.close()
    PostgresPool.put_connexion(conn)

    assert response.status == falcon.HTTP_204
    assert response.content == b''


def test_cities_406_error(client):
    """ Test en cas de retour json incompatible """
    response = client.simulate_get(
        TEST_VERSION + ROUTE_CITIES, headers=XML_ONLY_HEADER)
    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_city_ok(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_CITIES + '/Dijon')
    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert result == {'id': 217, 'name': 'Dijon'}


def test_city_dijon_406(client):
    response = client.simulate_get(
        TEST_VERSION + ROUTE_CITIES + '/Dijon', headers=XML_ONLY_HEADER)
    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_city_ko_case(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_CITIES + '/dijon')
    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_city_ko_unknown(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_CITIES + '/Paris')
    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_version(client):
    response = client.simulate_get(ROUTE_VERSION)
    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert result == {'version': '1.0', 'changelog': "Création de l'api"}


def test_version_406(client):
    response = client.simulate_get(ROUTE_VERSION, headers=XML_ONLY_HEADER)

    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_lines_ok(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/217')

    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert result == [LIGNE_T1_A,
                      LIGNE_T1_R,
                      LIGNE_T2_A,
                      LIGNE_T2_R,
                      LIGNE_COROL_A,
                      LIGNE_COROL_R,
                      LIGNE_CITY_A,
                      LIGNE_CITY_R]


def test_lines_ko_no_line(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/999')
    assert response.status == falcon.HTTP_204
    assert response.content == b''


def test_lines_ko_unknown(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/9999')
    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_lines_406(client):
    response = client.simulate_get(
        TEST_VERSION + ROUTE_LINES + '/217', headers=XML_ONLY_HEADER)

    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_lines_400(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/T217')

    assert response.status == falcon.HTTP_400
    assert response.content == b''

    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/T217/T1')

    assert response.status == falcon.HTTP_400
    assert response.content == b''


def test_line_ok(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/217/T1')

    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert result == [LIGNE_T1_A,
                      LIGNE_T1_R]


def test_line_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/217/T3')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_line_400(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/A/T3')

    assert response.status == falcon.HTTP_400
    assert response.content == b''


def test_line_406(client):
    response = client.simulate_get(
        TEST_VERSION + ROUTE_LINES + '/217/T3', headers=XML_ONLY_HEADER)

    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_line_direction_ok(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/217/T1/A')

    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert result == LIGNE_T1_A


def test_line_direction_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/9999/T1/B')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_line_direction_204(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_LINES + '/217/T1/B')

    assert response.status == falcon.HTTP_204
    assert response.content == b''


def test_line_direction_406(client):
    response = client.simulate_get(
        TEST_VERSION + ROUTE_LINES + '/217/T1/A', headers=XML_ONLY_HEADER)

    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_line_direction_400(client):
    response = client.simulate_get(
        TEST_VERSION + ROUTE_LINES + '/A/T1/A')

    assert response.status == falcon.HTTP_400
    assert response.content == b''


def test_stations_ok(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A')
    result = json.loads(response.content)

    assert response.status == falcon.HTTP_200
    assert len(result) == 4
    assert result[0] == {'code': 1542, 'nom': 'Auditorium', 'refs': '274400518|274399749|274401798'}
    assert result[1] == {'code': 1545, 'nom': 'Cap Vert', 'refs': '274400527|274399758|274401807'}
    assert result[2] == {'code': 1546, 'nom': 'CHU - Hôpitaux', 'refs': '274400522|274399753|274401802'}
    assert result[3] == {'code': 1462, 'nom': 'Darcy', 'refs': '274400515|274399746|274401795'}


def test_stations_400(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/A/T1/A')

    assert response.status == falcon.HTTP_400
    assert response.content == b''


def test_stations_ville_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/9999/T1/B')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_stations_ligne_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T3/A')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_stations_direction_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/B')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_stations_406(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A', headers=XML_ONLY_HEADER)

    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_stations_204(client):
    conn = PostgresPool.get_connexion()
    curs = conn.cursor()
    curs.execute("UPDATE station SET active = false")
    conn.commit()

    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A')

    curs.execute("UPDATE station SET active = true")
    conn.commit()
    curs.close()
    PostgresPool.put_connexion(conn)

    assert response.status == falcon.HTTP_204
    assert response.content == b''


def test_station_400(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/A/T1/A/1542')

    assert response.status == falcon.HTTP_400
    assert response.content == b''


def test_station_ville_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/9999/T1/B/1542')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_station_ligne_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T3/A/1542')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_station_direction_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/B/1542')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_station_station_404(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A/0')

    assert response.status == falcon.HTTP_404
    assert response.content == b''


def test_station_406(client):
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A/1542', headers=XML_ONLY_HEADER)

    assert response.status == falcon.HTTP_406
    assert response.content == b''


def test_time_remaining():
    assert StationRouteV1.get_time_remaining(datetime.now()) == 0
    assert StationRouteV1.get_time_remaining(datetime.now() + timedelta(minutes=10)) == 10
    assert StationRouteV1.get_time_remaining(datetime.now() + timedelta(minutes=20)) == 20
    assert StationRouteV1.get_time_remaining(datetime.strptime('2020-08-14 00:11', datetimeformat),
                                             datetime.strptime('2020-08-13 23:55', datetimeformat)) == 16
    assert StationRouteV1.get_time_remaining(datetime.strptime('2020-08-14 00:11', datetimeformat),
                                             datetime.strptime('2020-08-14 00:07', datetimeformat)) == 4
    assert StationRouteV1.get_time_remaining(datetime.strptime('2020-08-14 00:00', datetimeformat),
                                             datetime.strptime('2020-08-13 23:58', datetimeformat)) == 2
    assert StationRouteV1.get_time_remaining(datetime.strptime('2020-08-13 00:11', datetimeformat),
                                             datetime.strptime('2020-08-13 23:55', datetimeformat)) == 16
    assert StationRouteV1.get_time_remaining(datetime.strptime('2020-08-13 00:00', datetimeformat),
                                             datetime.strptime('2020-08-13 23:58', datetimeformat)) == 2


def test_station_ok(client, requests_mock):
    data_t1_a_capvert: str = open('../test/timeo_responses/T1_A_CapVert_station.xml').read()
    data_t1_a_capvert = data_t1_a_capvert.replace("<date>2020-08-08</date>", "<date>" + today + "</date>")
    data_t1_a_capvert = data_t1_a_capvert.replace("<duree>15:53</duree>", "<duree>" + ((datetime.now() + timedelta(minutes=12)).strftime('%H:%M')) + "</duree>")
    data_t1_a_capvert = data_t1_a_capvert.replace("<duree>16:05</duree>", "<duree>" + ((datetime.now() + timedelta(minutes=25)).strftime('%H:%M')) + "</duree>")

    requests_mock.get('http://timeo3.keolis.com/relais/217.php?xml=3&refs=274400527%7C274399758%7C274401807&ran=1', text=data_t1_a_capvert)
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A/1545')
    result = json.loads(response.content)

    assert response.status == falcon.HTTP_OK
    assert result == [{"time": 12, "destination": "QUETIGNY"}, {"time": 25, "destination": "QUETIGNY"}]


def test_station_ok_empty(client, requests_mock):
    data_t1_a_capvert: str = open('../test/timeo_responses/T1_A_CapVert_station_empty.xml').read()
    data_t1_a_capvert = data_t1_a_capvert.replace("<date>2020-08-08</date>", "<date>" + today + "</date>")
    requests_mock.get('http://timeo3.keolis.com/relais/217.php?xml=3&refs=274400527%7C274399758%7C274401807&ran=1', text=data_t1_a_capvert)
    response = client.simulate_get(TEST_VERSION + ROUTE_STATIONS + '/217/T1/A/1545')

    assert response.status == falcon.HTTP_204
    assert response.content == b''
