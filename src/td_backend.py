# -*- coding: utf-8 -*-
"""
Module backend pour l'application Transport Dijon

Ce module sert d'interface entre l'API keolis et les applications déployées sur Android
"""
import falcon

import bdd_migration
from postgres_pool import PostgresPool
from config import Config
from routes.messages import MessageRouteV1
from routes.cities import CitiesRouteV1
from routes.version import VersionRoute
from routes.lines import LinesRouteV1
from routes.stations import StationRouteV1

CURRENT_API_VERSION = "V1.0"

Config.load('config.json')
bdd_migration.main()

PostgresPool.init_pool_postgresql()

api = falcon.API()
api.add_route('/version', VersionRoute)
api.add_route('/'+CURRENT_API_VERSION+'/messages', MessageRouteV1)
api.add_route('/'+CURRENT_API_VERSION+'/cities', CitiesRouteV1, suffix='all')
api.add_route('/'+CURRENT_API_VERSION+'/cities/{city_name}', CitiesRouteV1)
api.add_route('/'+CURRENT_API_VERSION +
              '/lines/{city_id}', LinesRouteV1, suffix='all')
api.add_route('/'+CURRENT_API_VERSION+'/lines/{city_id}/{line}', LinesRouteV1)
api.add_route('/'+CURRENT_API_VERSION +
              '/lines/{city_id}/{line}/{direction}', LinesRouteV1, suffix='one')
api.add_route('/'+CURRENT_API_VERSION+'/stations/{city_id}/{line}/{direction}', StationRouteV1, suffix='all')
api.add_route('/'+CURRENT_API_VERSION+'/stations/{city_id}/{line}/{direction}/{station}', StationRouteV1)
