import json

from psycopg2 import pool


class PostgresPool:
    """
    Classe statique gérant le pool de connexion à la base de données
    """
    pg_pool: pool.ThreadedConnectionPool = None

    @staticmethod
    def init_pool_postgresql(config_file: str = 'config.json'):
        """
        Fonction d'initialisation du pool de connexions à la base de données
        """
        with open(config_file) as json_file:
            config = json.load(json_file)

        db_config = config['database']
        PostgresPool.pg_pool = pool.ThreadedConnectionPool(3, 5,
                                                           user=db_config['username'],
                                                           password=db_config['password'],
                                                           host=db_config['host'],
                                                           port="5432",
                                                           database=db_config['name'])

    @staticmethod
    def get_connexion():
        """
        Fonction permettant de récupérer une connexion du pool
        :return: Une connexion active à la base de données ou None en cas d'erreur
        """
        return PostgresPool.pg_pool.getconn()

    @staticmethod
    def put_connexion(connexion):
        """
        Permet de rendre au pool un connexion à la base de données
        :param connexion: connexion à la base de données à insérer dans le pool
        """
        PostgresPool.pg_pool.putconn(connexion)

    @staticmethod
    def close():
        """
        Fonction permettant de clore l'ensemble des connexions du pool
        """
        PostgresPool.pg_pool.closeall()

    @staticmethod
    def get_used_connection():
        """
        Fonction permettant de récupérer le nombre de connexions utilisées
        :return:
        """
        return len(PostgresPool.pg_pool._used)  # pylint: disable=protected-access
