# -*- coding: utf-8 -*-
import json
import sys
import os
import traceback


class Config():
    """
    Manage and store config
    """

    config_file = "config.json"
    configuration = {}

    @staticmethod
    def clear():
        """
        Function to clear config
        """
        Config.configuration = {}

    @staticmethod
    def load(file_path: str = config_file):
        """
        Read and load config from JSON file
        :param file_path:
        """
        Config.config_file = file_path
        print("Chargement de la configuration présente dans le fichier {file_name}".format(
            file_name=file_path))
        if os.path.exists(file_path):
            with open(file_path) as config_json:
                try:
                    Config.configuration = json.load(config_json)
                    print("Chargement de la configuration terminé")
                except json.decoder.JSONDecodeError:
                    print(
                        "Le fichier de configuration est n'est pas dans un json valide")
                    traceback.print_exc(file=sys.stderr)
                    sys.exit(2)
        else:
            print('Le fichier de configuration est introuvable', file=sys.stderr)
            sys.exit(1)
