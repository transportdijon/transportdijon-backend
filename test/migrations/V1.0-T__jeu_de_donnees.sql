TRUNCATE TABLE city CASCADE;
TRUNCATE TABLE message RESTART IDENTITY;

-- Ajout des villes
INSERT INTO city (id,nom) values ('105','Le mans');
INSERT INTO city (id,nom) values ('117','Pau');
INSERT INTO city (id,nom) values ('120','Soissons');
INSERT INTO city (id,nom) values ('135','Aix en provence');
INSERT INTO city (id,nom) values ('147','Caen');
INSERT INTO city (id,nom) values ('217','Dijon');
INSERT INTO city (id,nom) values ('297','Brest');
INSERT INTO city (id,nom) values ('402','Pau-Agen');
INSERT INTO city (id,nom) values ('422','St Etienne');
INSERT INTO city (id,nom) values ('440','Nantes');
INSERT INTO city (id,nom) values ('457','Montargis');
INSERT INTO city (id,nom) values ('497','Angers');
INSERT INTO city (id,nom) values ('691','Macon-Villefranche');
INSERT INTO city (id,nom) values ('910','EPINAY-SUR-ORGE');
INSERT INTO city (id,nom) values ('999','Rennes');

-- Ajout des lignes de test de Dijon
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (1,'T1','T1','A','QUETIGNY Centre','13369548','217','daily-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (2,'T1','T1','R','DIJON Gare','13369548','217','daily-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (3,'T2','T2','A','DIJON Valmy','13369548','217','daily-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (4,'T2','T2','R','CHENOVE Centre','13369548','217','daily-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (5,'Corol','C','A','Marmuzots','13382451','217','weekday-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (6,'Corol','C','R','Fontaine d&#39;Ouche','13382451','217','weekday-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (7,'City','DIV','A','Tivoli par Darcy','13369395','217','weekday-10h');
INSERT INTO line (id, name, code, sens, destination, color, city_id, schedule_reference) values (8,'City','DIV','R','République','13369395','217','weekday-10h');

-- Ajout des arrêts du T1 Aller
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (1,1542,'Auditorium',1,217,'274400518|274399749|274401798');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (2,1545,'Cap Vert',1,217,'274400527|274399758|274401807');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (3,1546,'CHU - Hôpitaux',1,217,'274400522|274399753|274401802');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (4,1462,'Darcy',1,217,'274400515|274399746|274401795');

-- Ajout des arrêts du T1 Retour
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (5,1562,'Auditorium',2,217,'274433036|274434316|274433548');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (6,1565,'Cap Vert',2,217,'274433027|274434307|274433539');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (7,1566,'CHU - Hôpitaux',2,217,'274433032|274434312|274433544');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (8,1494,'Darcy',2,217,'274433039|274434319|274433551');

-- Ajout des arrêts du T2 Aller
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (9,1474,'1er Mai',3,217,'274465283|274467079|274466823');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (10,1457,'Bourroches',3,217,'274465281|274467077|274466821');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (11,1469,'Carraz',3,217,'274467076|274466820');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (12,1459,'CHENÔVE Centre',3,217,'274467073|274466817');

-- Ajout des arrêts du T2 Retour
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (13,1507,'1er Mai',4,217,'274498575|274500111|274501135');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (14,1490,'Bourroches',4,217,'274498577|274500113|274501137');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (15,1502,'Carraz',4,217,'274498578|274502401|274500114|274502913');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (16,1459,'CHENÔVE Centre',4,217,'274498581|274502404|274500117|274502916');

-- Ajout des arrêts du Corol Aller
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (17,679,'1er Mai Foyer',5,217,'269027853');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (18,986,'Allobroges',5,217,'269027880');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (19,348,'Bachelard',5,217,'269027845');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (20,345,'Belin',5,217,'269027842');

-- Ajout des arrêts du Corol Retour
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (21,686,'1er Mai Foyer',6,217,'269060901');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (22,978,'Allobroges',6,217,'269060874');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (23,402,'Bachelard',6,217,'269060909');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (24,405,'Belin',6,217,'269060912');

-- Ajout des arrêts du City Aller
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (25,1678,'Beaux Arts',7,217,'270798342');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (26,1089,'Berbisey',7,217,'270798355');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (27,181,'Bibliothèque',7,217,'270798344');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (28,1681,'Bossuet',7,217,'270798347');

-- Ajout des arrêts du City Retour
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (29,1683,'Beaux Arts',8,217,'270829830');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (30,142,'Bibliothèque',8,217,'269060909');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (31,1093,'Cordeliers',8,217,'270829827');
INSERT INTO station (id, code, nom, line_id, city_id, refs)  values (32,1092,'Dumay',8,217,'270829826');

-- Ajout des messages
INSERT INTO message (criticality, title, "message", author, beginat, endat) values ('information', 'nouvelle application', 'Bienvenue sur la nouvelle application Transport Dijon','Tsuna', NOW(), '2020-12-31');