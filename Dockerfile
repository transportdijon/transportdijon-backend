FROM python:3.7.3

COPY docker/start.sh /app/start.sh
ADD src /app/
COPY test/config.json /app/config.json
WORKDIR /app
COPY docker/gunicorn_conf.py /app/gunicorn_conf.py
ENTRYPOINT ["/app/start.sh"]