# TransportDijon Backend

## info API : <https://pastebin.com/eEumTXw1>

## Fonctionnalité prévu

- [ ] Récupération de la liste des villes connu (dans un premier temps Dijon)
- [ ] Récupération de la liste complètes des lignes
- [ ] Récupération de la liste des arrêts de chaque ligne
- [ ] Récupération des horaires d'un arrêt
- [ ] Stockage en BDD serveur informations de Keolis
- [ ] Cron de synchro des lignes. (Dans la table des lignes, deux colone)


- [ ] Connexion via un compte google
- [ ] Stockage en BDD serveur des favoris des gens
- [ ] Synchro des appareils via les comptes google


## Les routes

Préfix des routes par un numéro de version. V1

- [x] GET /messages                                              - renvoie une liste de message de type annonce à affiche aux utilisateurs
- [x] GET /cities/                                               - liste des villes disponible 
- [x] GET /cities/{name}                                         - renvoie l'id de la ville si demandé
- [x] GET /lines/{city_id}/                                      - liste des lignes disponible
- [x] GET /lines/{city_id}/{line}                                - liste des directions pour la ligne demandé
- [ ] GET /stations/{city_id}/{line}/{direction}                 - liste des arrêt de la ligne demandé
- [ ] GET /stations/{city_id}/{line}/{direction}/{station}       - renvoie les horaires des prochains passage

- [ ] POST /connect                                              - permet de s'identifier
- [ ] POST /save                                                 - Enregistre les favoris passé dans le body au format json
- [ ] GET  /load                                                 - renvoie les favoris présent coté serveur
- [ ] POST /feedback                                             - Permet à l'utilisateur de transmettre un message à l'équipe de dev

Get Non versionné
- [x] GET /version                                               - Renvoie la version API et Packages afin de signaler à l'utilisateur une maj disponible


## Header

- [ ] Bearer que ce soit par compte google ou local


## Test local

Pour tester en local vous pouvez utiliser docker et docker-compose. La commande à utilisé pour démarrer est la suivantes :

```sh
docker-compose up --build
```
